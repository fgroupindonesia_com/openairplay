package Working;

import static Working.AirPlay.selectResolutionDialog;
import static Working.AirPlay.usage;
import static Working.AirPlay.searchDialog;
import etc.CmdLineParser;

/**
 *
 * @author asus
 */
public class Runner extends Thread {

    private String commandLine[] = new String[3];
    private AirPlay airplay = null;

    public void prepare() {
        commandLine[0] = "-h";
        commandLine[1] = Configuration.getIP();
        commandLine[2] = "-d";
    }

    public void end() {
        if (airplay != null) {
            airplay.stop();

        }
    }

    public void sleep() {
        if (airplay != null) {
            try {
                Thread.sleep(3000);
                airplay.pause();
            } catch (Exception ex) {
                System.err.println("something error while sleeping...");
            }
        }
    }

    public void run() {

        prepare();

        try {
            CmdLineParser cmd = new CmdLineParser();
            CmdLineParser.Option hostopt = cmd.addStringOption('h', "hostname");
            CmdLineParser.Option stopopt = cmd.addBooleanOption('s', "stop");
            CmdLineParser.Option photoopt = cmd.addStringOption('p', "photo");
            CmdLineParser.Option desktopopt = cmd.addBooleanOption('d', "desktop");
            CmdLineParser.Option passopt = cmd.addStringOption('a', "password");
            CmdLineParser.Option helpopt = cmd.addBooleanOption('?', "help");
            cmd.parse(commandLine);

            String hostname = (String) cmd.getOptionValue(hostopt);

            Boolean showHelp = (Boolean) cmd.getOptionValue(helpopt);

            if (null != showHelp && showHelp) {
                usage();
            } else if (hostname == null) { //show select dialog if no host address is given
                airplay = searchDialog(null);
                if (null != airplay) {
                    selectResolutionDialog(null, airplay);
                    airplay.desktop();
                }
            } else {

                String[] hostport = hostname.split(":", 2);
                if (hostport.length > 1) {
                    airplay = new AirPlay(hostport[0], Integer.parseInt(hostport[1]));
                } else {
                    airplay = new AirPlay(hostport[0]);
                }
                airplay.setAuth(new AirPlay.AuthConsole());
                String password = (String) cmd.getOptionValue(passopt);
                airplay.setPassword(password);
                String photo;
                if (cmd.getOptionValue(stopopt) != null) {
                    airplay.stop();
                } else if ((photo = (String) cmd.getOptionValue(photoopt)) != null) {
                    System.out.println("we're doing screen photo sharing.Press ctrl-c to quit");
                    airplay.photo(photo);
                } else if (cmd.getOptionValue(desktopopt) != null) {
                    System.out.println("we're doing desktop sharing.Press ctrl-c to quit");
                    airplay.desktop();
                } else {
                    usage();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
