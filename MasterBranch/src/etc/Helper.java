package etc;

import java.awt.Image;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Robot;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import javax.swing.ImageIcon;

/**
 *
 * @author asus
 */
public class Helper {

    // becarefull the name is case-sensitive
    private static String path = System.getProperty("user.home").toLowerCase() + "\\openAirplay";
    private static String imageOffPath = path + "\\off.png";
    private static String imageOnPath = path + "\\on.png";
    private static String imagePausePath = path + "\\pause.png";
    private static String imageCursorPath = path + "\\cursor.png";
    private static String imageAppPath = path + "\\logo.png";
    private static String fileMarker = path + "\\marker.dat";
    private static File fileConfig = new File(path + "\\config.inf");

    private static URL jarLocationUrl = Helper.class.getProtectionDomain().getCodeSource().getLocation();
    private static String jarLocation = new File(jarLocationUrl.toString()).getParent().replace("file:\\", "").replace("\\", "/");
    public static String jarName = "OpenAirPlay.jar";

    private static ImageIcon imageApp = new ImageIcon(imageAppPath, "Open Air Play");
    private static ImageIcon imageOff = new ImageIcon(imageOffPath, "Open Airplay State - Off.");
    private static ImageIcon imageOn = new ImageIcon(imageOnPath, "Open Airplay State - On.");
    private static ImageIcon imagePause = new ImageIcon(imagePausePath, "Open Airplay State - Paused.");
    private static ImageIcon imageCursor = new ImageIcon(imageCursorPath, "Mouse");

    private static Robot robot = null;
    
    public static Robot getCurrentRobot()throws Exception{
    
        if(robot==null){
            robot = new Robot();
        }
        
        return robot;
        
    }
    
    public static boolean isReady() {

        File dir = new File(fileMarker);
        if (dir.exists()) {
            // if the file still there it means its not ready yet
            dir.delete();
            return false;
        }
        return true;
    }

    public static File getFileConfig() {
        return fileConfig;
    }

    public static String getIconCursorPath() {
        return imageCursorPath;
    }
    
    public static Image getIconCursor() {
        return imageCursor.getImage();
    }

    public static int getCursorLocationX() {
        return MouseInfo.getPointerInfo().getLocation().x;
    }
    
    public static Point getCursorInfoPoint(){
        return MouseInfo.getPointerInfo().getLocation();
    }
    
    public static int getCursorLocationY(){
        return MouseInfo.getPointerInfo().getLocation().y;
    }

    public static Image getIconOff() {
        return imageOff.getImage();
    }

    public static Image getIconOn() {
        return imageOn.getImage();
    }
    
    public static Image getAppIcon() {
        return imageApp.getImage();
    }

    public static Image getIconPause() {
        return imagePause.getImage();
    }

    public static void prepareDirectoryFiles() {

        File target = new File(path);
        File marker = new File(fileMarker);

        if (target.exists() == false) {
            File targetGambarOn = new File(imageOnPath);
            File targetGambarOff = new File(imageOffPath);
            File targetGambarPause = new File(imagePausePath);
            File targetGambarMouse = new File(imageCursorPath);
            File targetGambarApp = new File(imageAppPath);

            try {
                target.mkdirs();
                marker.createNewFile();

                createFile(targetGambarOn);
                createFile(targetGambarOff);
                createFile(targetGambarPause);
                createFile(targetGambarMouse);
                createFile(targetGambarApp);

                System.err.println("AirPlay is ready!");

            } catch (Exception e) {
                PopupMessage.printOutError("Error while creating file directories!");

                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                e.printStackTrace(pw);
                PopupMessage.printOutError(sw.toString());

            }

        }
    }

    public static void createFile(File target) throws Exception {

        // becarefull the name is case-sensitive
        //JOptionPane.showMessageDialog(null, target.getName());
        FileOutputStream out = new FileOutputStream(target);

        // obtaining the filename from here
        System.err.println("Creating Images... - " + target.getName());
        //InputStream in = cl.getResourceAsStream("Images\\" + target.getName());
        // turn below code ON and turn upper code OFF
        // after compiled

        String fileURL = "jar:file:" + jarLocation + "/" + jarName + "!/" + target.getName();

        // for testing purposes only 
        //JOptionPane.showMessageDialog(null, fileURL);
        URL url = new URL(fileURL);

        InputStream in = url.openStream();

        byte[] buf = new byte[8 * 1024];
        int len;
        while ((len = in.read(buf)) != -1) {
            out.write(buf, 0, len);
        }
        out.close();
        in.close();

    }
}
