package etc;

import javax.swing.JOptionPane;

/**
 *
 * @author asus
 */
public class PopupMessage {

    public static void printOutError(String pesan) {

        JOptionPane.showMessageDialog(null, pesan, "Error", JOptionPane.ERROR_MESSAGE);

    }

    public static void printOutNormal(String pesan) {
        JOptionPane.showMessageDialog(null, pesan, "Status", JOptionPane.INFORMATION_MESSAGE);
    }

}
