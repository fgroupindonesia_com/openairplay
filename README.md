# README #

Petunjuk penggunaan Open Airplay (Developers Guide)

### INSTALASI ###

Ambil versi installer.msi atau portable.zip
Gunakan JRE 1.80 ke atas

### RUNNING ###

Cukup jalankan openAirplay.jar dan perhatikan penampakan di system tray

### KONFIGURASI ###

1. Pastikan komputer terhubung ke monitor-projector via wifi
2. Catat nomor IP mesin tersebut
3. Saat Open AirPlay telah running masuklah ke menu Configuration
4. Masukkan IP Address disana
5. Klik Activate

### KENDALA LAIN? ###

Hubungi tim http://fgroupindonesia.com